# Colour
Small planet generator in C#, with [SFML.Net (2.5.0)](https://www.sfml-dev.org/download/sfml.net/), and [Perlin noise](https://gist.github.com/Flafla2/1a0b9ebef678bbce3215).  
A semi-continuation/reboot of a project of mine in C++. ([link](https://github.com/sjaak31367/Colour))  

### Output
Example output (as of commit 8):  
![alt text](misc/example_output_01.png)

### Credits
[Perlin.cs](https://gist.github.com/Flafla2/1a0b9ebef678bbce3215) by [Flafla2](https://github.com/Flafla2)  
  
Font(s) used:  
[Courier Prime Bold](https://fonts.google.com/specimen/Courier+Prime?category=Monospace&selection.family=Courier+Prime:wght@700)  