﻿using SFML.Graphics;
using SFML.System;
using System;

namespace Colour
{
    class Planet
    {
        private uint planetRadius;
        private int seed;
        public double waterLevel = DefaultValues.waterLevel;
        public bool lockWaterLevel = DefaultValues.lockWaterLevel;
        private double[,] terrainCache2D;
        public Image planetImage;
        public Sprite planetSprite;



        // Constructors
        public Planet(uint radius)
        {
            planetRadius = radius;
            seed = DateTime.Now.GetHashCode();
            emptyCache();
            planetSprite = new Sprite();
            updateImageSize();
            //updateSprite();
        }

        public Planet(uint radius, int seed)
        {
            planetRadius = radius;
            this.seed = seed;
            emptyCache();
            planetSprite = new Sprite();
            updateImageSize();
            //updateSprite();
        }



        // Functions
        public void Generate()
        {
            //Clock clock = new Clock();
            emptyCache();
            Perlin.reseed(seed);
            double x, y, z;  // left-right, up-down, to-fro
            double r = planetRadius;
            for (uint pixel_y = 0; pixel_y < r * 2; pixel_y++) {
                for (uint pixel_x = 0; pixel_x < r * 2; pixel_x++) {
                    x = pixel_x - r;
                    y = pixel_y - r;
                    if ((x * x + y * y) <= r * r) {
                        z = Math.Sqrt(r * r - x * x - y * y);  // 0 ~ planetRadius
                        setHeight(pixel_x, pixel_y, (Perlin.OctavePerlin((x + r) / 2 / 100d, (y + r) / 2 / 100d, z / 100d, 4, 0.5d) - 0.4) / 1.2);  // (OctavePerlin( (x=0~r)/100, (y=0~r)/100, (z=0~r)/100, octaves, persistence)) -> 0.4~1.6 -> (0~1)
                    }
                }
            }
            //Console.WriteLine("Generated planet in {0}ms!", clock.ElapsedTime.AsMilliseconds());
        }

        public void Resize(uint radius)
        {
            //Console.WriteLine("Resizing planet: r{0} -> r{1}  (r*2= {2})", planetRadius, radius, radius*2);
            planetRadius = radius;
            updateImageSize();
            Generate();
            //Console.WriteLine("Resizing done:\n  cache: (y{0}, x{1})\n  image: (x{2}, y{3})\n  texture: (x{4}, y{5})", terrainCache2D.GetLength(0), terrainCache2D.GetLength(1), planetImage.Size.X, planetImage.Size.Y, planetSprite.Texture.Size.X, planetSprite.Texture.Size.Y);
        }

        public void Resize(uint radius, out string debugInfo)
        {
            debugInfo = string.Format("Resize:\n radius: {0} -> {1}\n diameter: {2} -> {3}", planetRadius, radius, planetRadius*2, radius*2);
            Resize(radius);
            debugInfo += string.Format("\n  --\n cache:   ({0} x {1})\n image:   ({2} x {3})\n texture: ({4} x {5})", terrainCache2D.GetLength(1), terrainCache2D.GetLength(0), planetImage.Size.X, planetImage.Size.Y, planetSprite.Texture.Size.X, planetSprite.Texture.Size.Y);
            debugInfo += string.Format("\n  --\n posX: {0}\n posY: {1}", Program.planet.planetSprite.Position.X, Program.planet.planetSprite.Position.Y);
        }

        public void emptyCache()
        {
            uint cacheSizeY = planetRadius * 2 + 2;
            uint cacheSizeX = planetRadius * 2 + 2;
            terrainCache2D = new double[cacheSizeY, cacheSizeX];
            for (uint y = 0; y < cacheSizeY; y++) {
                for (uint x = 0; x < cacheSizeX; x++) {
                    terrainCache2D[y, x] = double.NaN;
                }
            }
        }

        public void updateImageSize()
        {
            planetImage = new Image(planetRadius * 2 + 2, planetRadius * 2 + 2, new Color(0, 0, 0, 255));
            updateSprite();
        }

        public void updateSprite()
        {
            planetSprite.Texture = new Texture(planetImage);
            planetSprite.TextureRect = new IntRect(0, 0, (int) planetSprite.Texture.Size.X, (int) planetSprite.Texture.Size.Y);
            //Console.WriteLine("sprite transform  =  pos=({0}, {1}) scl=({2}, {3}) rot={4} ori=({5}, {6})", planetSprite.Position.X, planetSprite.Position.Y, planetSprite.Scale.X, planetSprite.Scale.Y, planetSprite.Rotation, planetSprite.Origin.X, planetSprite.Origin.Y);
            planetSprite.Position = new Vector2f((float) WindowHandler.getWindowSize().X / 2f - (float) planetImage.Size.X / 2f, (float) WindowHandler.getWindowSize().Y / 2f - (float) planetImage.Size.Y / 2f);
        }


        public ref double getHeight(uint pixel_X, uint pixel_Y)
        {
            return ref terrainCache2D[pixel_Y, pixel_X];
        }
        public void getHeight(uint pixel_X, uint pixel_Y, out double height)
        {
            height = terrainCache2D[pixel_Y, pixel_X];
        }

        public void setHeight(uint pixel_X, uint pixel_Y, double height)
        {
            terrainCache2D[pixel_Y, pixel_X] = height;
        }


        // Functions: Accessors
        public uint getPlanetRadius()
        {
            return planetRadius;
        }
    }
}
