﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;

namespace Colour
{
    static class WindowHandler
    {
        public static Font font = new Font("fonts/CourierPrime-Bold.ttf");
        public static string resizeDebugInfo = "";
        public static RenderWindow window;
        private static Vector2i mousePos = new Vector2i(0, 0);

        // EventHandlers EventHandlers
        public static EventHandler closeEventHandler = new EventHandler(OnClose);
        public static EventHandler<KeyEventArgs> keyPressEventHandler = new EventHandler<KeyEventArgs>(OnKeyPress);
        public static EventHandler<KeyEventArgs> keyReleaseEventHandler = new EventHandler<KeyEventArgs>(OnKeyRelease);
        public static EventHandler<SizeEventArgs> resizeEventHandler = new EventHandler<SizeEventArgs>(OnResize);
        public static EventHandler<MouseMoveEventArgs> mouseMoveEventHandler = new EventHandler<MouseMoveEventArgs>(OnMouseMove);



        // EventHandlers Functions
        public static void OnClose(object sender, EventArgs e) {
            RenderWindow window = (RenderWindow) sender;
            window.Close();
        }

        public static void OnKeyPress(object sender, KeyEventArgs e) {
            RenderWindow window = (RenderWindow) sender;
            if (e.Code == Keyboard.Key.Escape) {
                window.Close();
            }
            // input for movement goes here
        }

        public static void OnKeyRelease(object sender, KeyEventArgs e) {
            RenderWindow window = (RenderWindow) sender;
            if (e.Code == Keyboard.Key.Space) {
                Program.planet.lockWaterLevel = !Program.planet.lockWaterLevel;
            }
            if (e.Code == Keyboard.Key.R) {
                Program.planet.lockWaterLevel = DefaultValues.lockWaterLevel;
                Program.planet.waterLevel = DefaultValues.waterLevel;
                window.Size = new Vector2u(DefaultValues.windowSizeX, DefaultValues.windowSizeY);
                window.DispatchEvents();  // Call all events that were triggered by the resize, and trigger them now. Because otherwise they'll fire after we're done, and thus recalculate planet Resize stuff, overwriting what we're doing below.
                Program.planet.Resize(DefaultValues.planetRadius, out resizeDebugInfo);
            }
        }

        public static void OnResize(object sender, SizeEventArgs e) {
            Program.planet.Resize(Math.Min(e.Width, e.Height) / 2, out resizeDebugInfo);
            //((RenderWindow) sender).Clear(new Color(0, 0, 255, 255));
            //Program.planet.planetSprite.Scale = new Vector2f(1f, 1f);
            ((RenderWindow) sender).SetView(new View(new Vector2f(e.Width/2, e.Height/2), new Vector2f(e.Width, e.Height)));
            //Console.WriteLine("RESIZE DONE!");
        }

        public static void OnMouseMove(object sender, MouseMoveEventArgs e) {
            mousePos = new Vector2i(e.X, e.Y);
        }



        // Functions: Accessors
        public static Vector2u getWindowSize() {
            return new Vector2u(window.Size.X, window.Size.Y);
        }

        public static void setWindow(ref RenderWindow window) {
            WindowHandler.window = window;
        }

        public static Vector2i getMousePos() {
            return mousePos;
        }
    }
}
