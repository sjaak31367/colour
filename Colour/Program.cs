﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;

namespace Colour
{
    class Program
    {
        public const int targetFPS = 60;
        public const float targetFrameTime = 1000 / targetFPS;
        public static uint frameNumber = 0;
        public static Planet planet;
        public static Color[] lut = new Color[256];  // LookUpTable



        static void Main(string[] args)
        {
            // Command Line Args
            /*if (args.Length == 1) {
                bool succesfullInput = true;
                if (!uint.TryParse(args[1], out planetRadius)) {
                    succesfullInput = false;
                }
                if (!succesfullInput) {
                    Console.WriteLine("!WARNING! Could not pass input arg(s), going with default arg(s)!");
                }
            }*/

            // Print controls
            Console.WriteLine("Controls:");
            Console.WriteLine("  Mouse:");
            Console.WriteLine("    Move up/down :  Raise/lower waterlevel");
            Console.WriteLine("  Keyboard:");
            Console.WriteLine("    [Space]      :  Lock/unlock waterlevel");
            Console.WriteLine("    [R]          :  Reset ALL (Window stuff (size)) + (Planet stuff(waterlevel, camera position, zoom, etc.))");


            // Start window
            RenderWindow window = new RenderWindow(new VideoMode(DefaultValues.windowSizeX, DefaultValues.windowSizeY), "Colour Planets!");
            WindowHandler.setWindow(ref window);

            // Attach event listeners
            window.Closed += WindowHandler.closeEventHandler;
            window.KeyPressed += WindowHandler.keyPressEventHandler;
            window.KeyReleased += WindowHandler.keyReleaseEventHandler;
            window.Resized += WindowHandler.resizeEventHandler;
            window.MouseMoved += WindowHandler.mouseMoveEventHandler;


            // Start planet
            planet = new Planet(DefaultValues.planetRadius, DateTime.Now.GetHashCode());  // TODO SOMEWHY THIS LITTERAL HAS LASTING EFFECT ON SIZES (specifically, the image size)
            planet.Generate();
            GC.Collect();
            #region oldCode
            //Console.WriteLine(Perlin.OctavePerlin(2, 5, 7, 2, 0.5));
            /*double[,,] noise = new double[planetRadius * 2, planetRadius * 2, planetRadius * 2];  // Generate a chunk of Perlin noise
            for (int x = 0; x < planetRadius*2; x++) {
                for (int y = 0; y < planetRadius*2; y++) {
                    for (int z = 0; z < planetRadius*2; z++) {
                        noise[x, y, z] = Perlin.OctavePerlin(x / (planetRadius * 2d), y / (planetRadius * 2d), z / (planetRadius * 2d), 4, 0.5) -0.5;
                        //Console.WriteLine(noise[x, y, z]);
                        //noise[x, y, z] = Perlin.OctavePerlin(x, y, z, 4, 0.5);
                        //Console.WriteLine(noise[x, y, z]);
                    }
                }
            }*/
            #endregion


            Clock clock = new Clock();
            Clock frameStartTime = new Clock();
            long frameDurationTime = 0;
            int timeOfLastFrame = 0;

            // Main render-loop
            Text FPSDiplay = new Text("", WindowHandler.font, 16);
            Text debugDisplay = new Text("", WindowHandler.font, 14);
            debugDisplay.Position = new Vector2f(4, 24);
            Text mouseDebugDiplay = new Text("", WindowHandler.font, 14);
            Console.WriteLine("Starting drawing!");
            while (window.IsOpen) {
                window.DispatchEvents();

                if (frameNumber == 0) {
                    planet.Resize(planet.getPlanetRadius(), out WindowHandler.resizeDebugInfo);
                }

                if (clock.ElapsedTime.AsMilliseconds() >= timeOfLastFrame + targetFrameTime) {
                    frameStartTime.Restart();
                    timeOfLastFrame = clock.ElapsedTime.AsMilliseconds();

                    #region brokenCode
                    //planetImage.SetPixel(1, 1, new Color(0, 255, 0, 255));
                    /*for (uint x = 0; x < planetRadius * 2; x++) {
                        for (uint y = 0; y < planetRadius*2; y++) {
                            //planetImage.SetPixel(x, y, new Color((byte) (noise[x, y, frameNumber % (planetRadius * 2)] * 255), 0, 0, 255));
                            //planetImage.SetPixel(x, y, new Color((byte) ((Perlin.OctavePerlin(x / (planetRadius * 2d), y / (planetRadius * 2d), frameNumber*10 % (planetRadius * 2) / (planetRadius * 2d), 4, 0.5) - 0.5) * 255), 0, 0, 255));
                            planetImage.SetPixel(x, y, new Color( (byte) ((Perlin.OctavePerlin(x / (planetRadius * 1d), y / (planetRadius * 1d), frameNumber * 10 % (planetRadius * 2) / (planetRadius * 1d), 4, 0.5) - 0.4) / 1.2 * 255 )  , 0, 0, 255));
                            //planetImage.SetPixel(x, y, new Color((byte) ((Perlin2.Noise(x / (planetRadius * 2f), y / (planetRadius * 2f), frameNumber * 10 % (planetRadius * 2) / (planetRadius * 2f)) + 1) * 127), 0, 0, 255));
                        }
                    }*/
                    /*double x;
                    double y;
                    for (uint pixelY = 0; pixelY < 50; pixelY++) {
                        for (uint pixelX = 0; pixelX < 50; pixelX++) {
                            x = (pixelX / 50d) * Math.PI;
                            y = (pixelY / 50d) * Math.PI;
                            planetImage.SetPixel(pixelX, pixelY, new Color((Byte) (Math.Sin(x)*Math.Sin(y)*255),0,0,255));
                        }
                    }*/
                    /*double radius = Math.PI;
                    for (double x = -Math.PI; x < radius; x += 0.1) {
                        for (double y = -Math.PI; y < radius; y += 0.1) {
                            for (double z = -Math.PI; z < radius; z += 0.1) {
                                if ((int) (Math.Sqrt(Math.Sqrt(x*x + y*y) + z*z)*10) == (int) (radius*10)) {
                                    //Console.WriteLine("{0}, {1}, {2}", x,y,z);
                                    planetImage.SetPixel((uint) ((x + Math.PI) * 50), (uint) ((y + Math.PI) * 50), new Color((Byte) (Math.Sin(z)*255), 0, 0, 255));  // 0~2pi -> 0~100
                                }
                            }
                        }
                    }*/
                    /*double u_max = Math.PI * 2;
                    double v_max = Math.PI;
                    double x, y, z;
                    for (double u = 0; u < u_max; u += 0.1) {
                        for (double v = 0; v < v_max; v += 0.1) {
                            x = Math.Cos(u) * Math.Sin(v);
                            y = Math.Sin(u) * Math.Sin(v);
                            z = Math.Cos(v);
                            //Console.WriteLine("{0} {1} {2}", x, y, z);
                            planetImage.SetPixel((uint) ((x+2) * 10), (uint) ((y+2) * 10), new Color((Byte) (z * 50), 0, 0, 255));
                        }
                    }*/
                    /*double z;
                    //double r = 1;// planetRadius / Math.PI;
                    for (uint y = 0; y < planetRadius*2; y++) {
                        for (uint x = 0; x < planetRadius * 2; x++) {
                            if (Math.Sqrt(x * x + y * y) <= planetRadius+72) {
                                Byte r = 1;
                                //z = Math.Sqrt(r * r - x * x - y * y); Console.WriteLine(z);
                                //z = Math.Sqrt(1 * 1 - x * x - y * y); Console.WriteLine(z);
                            
                                z = Math.Sqrt(r - x * x - y * y); //Console.WriteLine((int) (z*255) >> 16);
                                //Console.WriteLine();
                                //bool stop = true;
                                / * if (x > 4) {
                                    while (true) ;
                                } * /
                                planetImage.SetPixel(x, y, new Color((Byte) (z*255), 0, 0, 255));
                            }
                        }
                    }*/
                    #endregion

                    // Generate LookUpTable
                    if (!planet.lockWaterLevel) {
                        planet.waterLevel = 255d / window.Size.Y * -WindowHandler.getMousePos().Y + 255d;  // bottom of screen = low water. top of screen = high water
                    }
                    for (uint i = 0; i < 256; i++) {
                        if (i < planet.waterLevel) {
                            lut[i] = new Color(0, 0, (byte) i, 255);
                        } else {
                            lut[i] = new Color(0, (byte) i, 0, 255);
                        }
                    }
                    mouseDebugDiplay.DisplayedString = string.Format("Mouse:\n x: {0}\n y: {1}\n waterlevel: {2}\n lockWaterLevel: {3}", WindowHandler.getMousePos().X, WindowHandler.getMousePos().Y, planet.waterLevel, planet.lockWaterLevel);

                    double x, y;  // left-right, up-down
                    double r = planet.getPlanetRadius();
                    Color color;
                    byte val;
                    for (uint pixel_y = 0; pixel_y < r * 2; pixel_y++) {
                        for (uint pixel_x = 0; pixel_x < r * 2; pixel_x++) {
                            x = (double) pixel_x - r;
                            y = (double) pixel_y - r;
                            if ((x*x + y*y) <= r*r) {
                                val = (byte) (planet.getHeight(pixel_x, pixel_y) * 255d);  // 0 ~ 255
                                color = lut[val];//new Color(val, val, val, 255);
                                planet.planetImage.SetPixel(pixel_x, pixel_y, color);
                                //planetImage.SetPixel(pixel_x, pixel_y, new Color((byte) (z / r * 255), 0, 0, 255));
                            }
                        }
                    }
                    planet.planetSprite.Texture = new Texture(planet.planetImage);

                    FPSDiplay.DisplayedString = (frameDurationTime / 1000.0).ToString() + " ms";

                    debugDisplay.DisplayedString = WindowHandler.resizeDebugInfo;

                    mouseDebugDiplay.Position = new Vector2f(4, debugDisplay.FindCharacterPos((uint) (debugDisplay.DisplayedString.Length) - 1).Y + debugDisplay.Position.Y + debugDisplay.CharacterSize);  // Just under the resizeDebugDisplay

                    window.Clear();
                    window.Draw(planet.planetSprite);
                    window.Draw(FPSDiplay);
                    window.Draw(debugDisplay);
                    window.Draw(mouseDebugDiplay);
                    window.Display();

                    ++frameNumber;
                    frameDurationTime = frameStartTime.ElapsedTime.AsMicroseconds();
                }

                if (frameNumber % targetFPS == 1) {  // Every (hopefully) 1 second. But also after the program's first rendered frame.
                    GC.Collect();
                }
            }
        }
    }
}
