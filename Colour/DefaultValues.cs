﻿using SFML.System;

namespace Colour
{
    static class DefaultValues
    {
        // Window stuff
        public const uint windowSizeX = 800;
        public const uint windowSizeY = 600;

        // Planet stuff
        public const uint planetRadius = 290;
        public const double waterLevel = 127;
        public const bool lockWaterLevel = true;
    }
}
